"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importDefault(require("chai"));
var server = require('../src/server');
let expect = chai_1.default.expect;
describe("test Name", function () {
    it('tester le nom', function () {
        var nom = "toto";
        expect(nom).equals("toto");
    });
});
