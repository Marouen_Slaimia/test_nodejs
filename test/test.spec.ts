import chai from 'chai';

var server = require('../src/server')

let expect = chai.expect;

describe("test Name", function() {
    it ("Tester le nom", function(){
        var msg = server.insertName("Alice");
        expect(msg).to.equal("Le nom est Alice");
    });
});
