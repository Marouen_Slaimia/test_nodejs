var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use('/html', express.static(__dirname+'/html'));
app.get('/', function (req, res, next) {
    console.log("on demande index.html");
    res.redirect('/html/index.html');
});

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.post('/insererNom', function (req, res, next) {
    var nom = req.body.nom;
    insertName(nom);
    console.log("variable nom : "+nom);
    res.send('{"nom":"'+nom+'"}');
});

exports.insertName = function (nom){
    var msg = 'Le nom est ' + nom;
    return msg;
}
app.listen(8000, function () {
    console.log("simple express node server listening at 8000");
});
